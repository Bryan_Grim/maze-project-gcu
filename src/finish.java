//Finish search moveset

public class finish {
	
	java.awt.Point startingPoint = new java.awt.Point();

	public static boolean findFinish(int x, int y){
		mainMap.printProgress();
		System.out.println();
		if(mainMap.reachedFinish(x,y))
			return true;	
		
		else if(mainMap.hitWall(x,y)){
			{
				mainMap.startingPoint.x = x;
				mainMap.startingPoint.y = y;
			return false;
			}
		}
		else{
			// Mark space traveled
			Maze.mazeMap[y][x] = '*';
			mainMap.startingPoint.y = y;
			mainMap.startingPoint.x = x;
			
			
			if (y - 1 >= 0 && findFinish(x, y-1)){ //Check SOUTH for open space(.)
				return true;
			}
				else if(x + 1 < Maze.mazeMap[0].length && findFinish(x + 1, y)){ //Check EAST for open space(.)
					return true;
				}
					else if(y + 1 < Maze.mazeMap.length && findFinish(x, y + 1)){ //Check NORTH for open space(.)
						return true;
					}
						else if(x - 1 >= 0 && findFinish(x - 1, y)){//Check WEST for open space(.)
							return true;
						}
							else{
								Maze.mazeMap[y][x] = '*';
								mainMap.startingPoint.x = x;
								mainMap.startingPoint.y = y;
								
								return false;
							}
		} 
	}
}
