public class mainMap {
	
	    static java.awt.Point startingPoint = new java.awt.Point();// Map position Pointer	
	   
		public mainMap(){
			for(int y = 0; y < Maze.mazeMap.length; y++){ //Find Start (1,1)
				for(int x = 0; x < Maze.mazeMap[y].length; x++){
					if(Maze.mazeMap[y][x] == 'S'){
						startingPoint.x = x;
						startingPoint.y = y;
					}
				}
			}
			//Check if Finish (F) is found
			System.out.println("Finish line found = " + finish.findFinish(startingPoint.x, startingPoint.y));
		}
		
		public static boolean reachedFinish(int x, int y){
			return Maze.mazeMap[y][x] == 'F';
		}
		
		public static boolean hitWall(int x, int y){
			return ".X".indexOf(Maze.mazeMap[y][x]) != -1;
		}
		public static void printProgress(){
			for(int y = 0; y < Maze.mazeMap.length; y++){ 
				for(int x = 0; x < Maze.mazeMap[y].length; x++){
					System.out.print(Maze.mazeMap[y][x]);
				}
				System.out.println();
			}
		}
	
		
}
